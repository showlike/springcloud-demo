package com.dreamer.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dreamer.feign.SchedualServiceHiInter;

@RestController
public class HiController {

    @Autowired
    SchedualServiceHiInter schedualServiceHiInter;
    
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    public String sayHi(@RequestParam String name){
        return schedualServiceHiInter.sayHiFromClientOne(name);
    }
}